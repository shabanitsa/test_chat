class CreateChats < ActiveRecord::Migration
  def change
    create_table :chats do |t|
      t.string :name, null: false
    end
  end
end
