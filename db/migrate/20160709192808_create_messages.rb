class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :text,           null: false
      t.belongs_to :user, index: true, foreign_key: true
      t.belongs_to :chat, index: true, foreign_key: true
      t.datetime :created_at,   null: false
    end
  end
end
