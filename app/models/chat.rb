class Chat < ActiveRecord::Base

  has_many :messages
  has_and_belongs_to_many :users, dependent: :destroy, before_remove: :clear_unreaded_messages
  has_many :unreaded_messages, through: :users


  accepts_nested_attributes_for :users

  validate :validate_users_count

  default_scope {
    order(id: :desc)
  }

  # Минимальное количество участников
  MIN_USERS_COUNT = 2


  def mark_as_readed_for(_user)
    msgs = _user.unreaded_messages.where(chat_id: self.id)
    _user.unreaded_messages.delete(msgs)
  end


  private


    def clear_unreaded_messages(leaving_user)
      self.mark_as_readed_for(leaving_user)
    end


    def validate_users_count
      if self.users.size < MIN_USERS_COUNT
        self.errors[:user_ids] << "Need at least #{MIN_USERS_COUNT} users for chat"
      end
    end

end
