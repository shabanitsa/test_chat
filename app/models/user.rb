class User < ActiveRecord::Base

  has_many :messages
  has_and_belongs_to_many :chats
  has_and_belongs_to_many :unreaded_messages, class_name: 'Message'

  validates :name, presence: true, uniqueness: true


  after_initialize do
    self.messages_count ||= 0
  end


  def self.authorize(login, pswd)
    # TODO: secure password
    User.where(name: login, password: pswd).first
  end


  def get_chat_by_id(chat_id)
    self.chats.where( chats: { id: chat_id } ).first
  end


  def new_chat(chat_params)
    chat = Chat.new(chat_params)
    chat.users << self
    chat
  end


  def new_message(chat, message_params)
    msg = self.messages.build(message_params)
    msg.chat_id = chat.id
    msg
  end


  def unreaded_messages_for(chat)
    self.unreaded_messages.where(chat: chat)
  end


  def unreaded_messages_count_for(chat)
    unreaded_messages_for(chat).count
  end

end
