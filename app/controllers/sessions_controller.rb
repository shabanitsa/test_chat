
class SessionsController < ApplicationController

  skip_before_filter :ensure_authenticated_user


  def create
    @current_user = User.authorize user_params[:name], user_params[:password]

    respond_to do |format|
      if @current_user
        # для API лучше было бы использовать token ?
        # reset_session
        # session[user_id:] = @current_user.id
        format.json { render json: @current_user, status: :created }
      else
        format.json { render json: {}, status: :unauthorized }
      end
    end
  end


  private


    def user_params
      params.require(:user).permit(
        :name,
        :password  # ? в примерах отсутствует, хотя в функционале указан
      )
    end

end
