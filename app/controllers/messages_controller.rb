
class MessagesController < ApplicationController

  before_filter :get_chat

  def index
    @messages = @chat.messages
  end


  def create
    @message = @current_user.new_message(@chat, message_params)
    @message.save
    # respond_to do |format|
    #   if @message.save
    #     format.json { render json: @message, status: :created }
    #   else
    #     format.json { render json: @message.errors, status: :unprocessable_entity }
    #   end
    # end
    render :show
  end


  def unreaded
    @messages = @current_user.unreaded_messages.where(chat_id: @chat.id)
    # respond_to do |format|
    #   format.json { render json: @messages, status: :created }
    # end
    render :index
  end


  private


    def get_chat
      @chat = @current_user.get_chat_by_id(params[:chat_id]) || raise(ActiveRecord::RecordNotFound, "Chat not found or user is not in chat")
    end


    def message_params
      params.require(:message).permit(
        :text
      )
    end

end
