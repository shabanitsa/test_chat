
class ChatsController < ApplicationController

  before_filter :get_chat, except: [:index, :create]

  def index
    @chats = @current_user.chats.includes(:users)
  end


  def create
    @chat = @current_user.new_chat(chat_params)
    @chat.save
    render :show
  end


  def update
    @chat.update_attributes(chat_params)
    render :show
  end


  def mark_as_readed
    @chat.mark_as_readed_for(@current_user)
    render :show
    # respond_to do |format|
    #   format.json { render json: {}, status: :ok }
    # end
  end


  private


    def get_chat
      @chat = @current_user.get_chat_by_id(params[:id]) || raise(ActiveRecord::RecordNotFound, "Chat not found or user is not in chat")
    end


    def chat_params
      params.require(:chat).permit(
        :name,
        user_ids:  []
      )
    end

end
