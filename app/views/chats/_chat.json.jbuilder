
json.chat {
  json.id chat.id
  json.name chat.name
  json.users_ids chat.user_ids
  json.unreaded_messages_count @current_user.unreaded_messages_count_for(chat)
}
