
json.message {
  json.id message.id
  json.text message.text
  json.author message.user.name
  json.created_at message.created_at
}
