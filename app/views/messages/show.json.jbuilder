
if @message.errors.empty?
  json.partial! 'message', message: @message
else
  json.errors do
    json.array! @message.errors
  end
end
