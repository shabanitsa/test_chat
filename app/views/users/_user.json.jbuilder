
json.user {
  json.id user.id
  json.name user.name
  json.messages_count user.unreaded_messages.count
  json.chats user.chat_ids
}
