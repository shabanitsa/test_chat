FactoryGirl.define do
  factory :chat do
    name { Faker::Name.title }
  end
end
