FactoryGirl.define do
  factory :user do
    name { Faker::Name.name }
    password { Faker::Internet.password(3,3) }
  end
end
