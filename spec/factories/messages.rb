FactoryGirl.define do
  factory :message do
    text { Faker::Lorem.sentence }
    association :user
    association :chat
  end
end
