#encoding: utf-8

require 'rails_helper'

describe MessagesController, type: :controller do
  before :each do
    @ivan = create(:user)
    @petr = create(:user)
    @olga = create(:user)

    @ivan_and_olga_chat  = @ivan.new_chat({ name: 'Love',         user_ids: [@olga.id] })
    @petr_and_olga_chat  = @petr.new_chat({ name: 'Dіvorce',      user_ids: [@olga.id] })
    @common_chat         = @petr.new_chat({ name: 'Our triangle', user_ids: [@olga.id, @ivan.id] })
    @ivan_and_olga_chat.save!
    @petr_and_olga_chat.save!
    @common_chat.save!
  end


  context 'receive message' do
    before :each do
      @msg_from_ivan = @ivan.new_message(@common_chat,  text: "Hello!" )
      @msg_from_petr = @petr.new_message(@common_chat,  text: "Hi!"    )
      @msg_from_ivan.save!
      @msg_from_petr.save!
    end

    describe 'GET #index' do
      it 'return all messages in chat for user' do
        get :index, chat_id: @common_chat, user_id_token: @olga.id, format: :json
        expect(assigns(:messages)).to match_array [@msg_from_ivan, @msg_from_petr]
      end

      it 'renders the :index template' do
        get :index, chat_id: @common_chat, user_id_token: @olga.id, format: :json
        expect(response).to render_template :index
      end
    end

    describe 'GET #unreaded' do
      it 'returns all unreaded messages for user in chat' do
        @common_chat.mark_as_readed_for @olga
        new_msg_from_petr = @petr.new_message(@common_chat, text: "I hate you!")
        new_msg_from_petr.save!
        get :unreaded, chat_id: @common_chat, user_id_token: @olga.id, format: :json
        expect(assigns(:messages)).to match_array [new_msg_from_petr]
      end
    end
  end


  describe 'POST #create' do
    it 'saves the new message for chat in the database' do
      expect{
        post :create, chat_id: @common_chat, user_id_token: @olga.id, message: { text: 'Hi, guys!' }, format: :json
      }.to change(Message, :count).by(1)
    end
  end

end
