#encoding: utf-8

require 'rails_helper'

describe ChatsController, type: :controller do
  before :each do
    @ivan = create(:user)
    @petr = create(:user)
    @olga = create(:user)
  end


  describe 'GET #index' do
    before :each do
      @ivan_and_olga_chat  = @ivan.new_chat({ name: 'Love',         user_ids: [@olga.id] })
      @petr_and_olga_chat  = @petr.new_chat({ name: 'Dіvorce',      user_ids: [@olga.id] })
      @common_chat         = @petr.new_chat({ name: 'Our triangle', user_ids: [@olga.id, @ivan.id] })
      @ivan_and_olga_chat.save!
      @petr_and_olga_chat.save!
      @common_chat.save!
    end

    it 'return all chats list' do
      get :index, user_id_token: @olga.id, format: :json
      expect(assigns(:chats)).to match_array [@ivan_and_olga_chat, @petr_and_olga_chat, @common_chat]
    end

    it 'renders the :index template' do
      get :index, user_id_token: @olga.id, format: :json
      expect(response).to render_template :index
    end
  end


  describe 'POST #create' do
    context 'with valid attributes' do
      it 'saves the chat in the database' do
        expect{
          post :create, user_id_token: @ivan.id, chat: { name: 'New chat', user_ids: [@olga.id, @petr.id] }, format: :json
        }.to change(Chat, :count).by(1)
      end
    end

    context 'with invalid attributes' do
      it 'don''t saves the chat with too few users count' do
        expect{
          post :create, user_id_token: @ivan.id, chat: { name: 'Chat without other users', user_ids: [] }, format: :json
        }.to_not change(User, :count)
      end
    end
  end


  context 'change existing chat' do
    before :each do
      @ivan_and_petr_chat = @ivan.new_chat({ name: 'Fight club', user_ids: [@petr.id] })
      @ivan_and_petr_chat.save!
    end

    describe 'PATCH #update' do
      context 'valid attributes' do
        it 'changes chat name' do
          patch :update, id: @ivan_and_petr_chat, user_id_token: @ivan.id, chat: { name: 'Football' }, format: :json
          @ivan_and_petr_chat.reload
          expect(@ivan_and_petr_chat.name).to eq('Football')
        end

        it 'changes chat users' do
          patch :update, id: @ivan_and_petr_chat, user_id_token: @ivan.id, chat: { user_ids: [@ivan.id, @petr.id, @olga.id] }, format: :json
          @ivan_and_petr_chat.reload
          expect(@ivan_and_petr_chat.users).to match_array [@ivan, @petr, @olga]
        end
      end

      context 'invalid attributes' do
        it 'don''t changes chat name of users list if user not in chat' do
          patch :update, id: @ivan_and_petr_chat, user_id_token: @olga.id, chat: { name: 'Cats and flowers', user_ids: [@ivan.id, @petr.id, @olga.id] }, format: :json
          @ivan_and_petr_chat.reload
          expect(@ivan_and_petr_chat.name).to_not eq('Cats and flowers')
          expect(@ivan_and_petr_chat.name).to_not match_array [@ivan, @petr, @olga]
        end
      end
    end


    describe 'PATCH #mark_as_readed' do
      it 'mark chat (users messages in chat) as readed only for this user' do
        @ivan.new_message(@ivan_and_petr_chat, text: "Let's rock!").save!
        @petr.new_message(@ivan_and_petr_chat, text: "Ok!"        ).save!
        patch :mark_as_readed, id: @ivan_and_petr_chat, user_id_token: @petr.id, format: :json
        expect(@petr.unreaded_messages_count_for(@ivan_and_petr_chat)).to be_zero
        expect(@ivan.unreaded_messages_count_for(@ivan_and_petr_chat)).to eq 1
      end
    end
  end

end
