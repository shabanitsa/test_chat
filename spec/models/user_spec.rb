#encoding: utf-8

require 'rails_helper'

describe User do

  it 'может зарегистрироваться введя имя, если имя не уникальное то пользователю получает уведомление об этом' do
    create(:user, name: 'Same Name')
    user = build(:user, name: 'Same Name')
    user.valid?
    expect(user.errors[:name]).to include('has already been taken')
  end


  it 'может авторизоваться введя свое имя и пароль' do
    user_attributes = attributes_for(:user)
    user = User.create(user_attributes)
    authorized_user = User.authorize(user_attributes[:name], user_attributes[:password])
    expect(authorized_user).to eq user
  end


  it 'может создать чат пригласив туда одного или нескольких участников' do
    chat_creator = create(:user)
    chat_guest1 = create(:user)
    chat_params = {
      name: 'New chat',
      user_ids: [chat_guest1.id]
    }
    chat = chat_creator.new_chat(chat_params)
    expect(chat).to be_valid
  end


  it 'не может создать чат с менее чем 2 участниками' do
    chat_creator = create(:user)
    chat_params = {
      name: 'Bad chat'
    }
    chat = chat_creator.new_chat(chat_params)
    chat.valid?
    expect(chat.errors[:user_ids]).to include('Need at least 2 users for chat')
  end


  context 'редактирование чата' do

    before :each do
      @chat_creator = create(:user)
      @chat_guest1 = create(:user)
      chat_params = {
        name: 'New chat',
        user_ids: [@chat_guest1.id]
      }
      @chat = @chat_creator.new_chat(chat_params)
      @chat.save
    end


    it 'может изменить название чата' do
      edit_chat = @chat_creator.get_chat_by_id(@chat.id)
      edit_chat.update_attributes(name: 'Another chat name')
      expect(edit_chat.name).to eq 'Another chat name'
    end


    it 'может изменить список участников чата' do
      chat_guest2 = create(:user)
      edit_chat = @chat_creator.get_chat_by_id(@chat.id)
      edit_chat.update_attributes(user_ids: [ @chat_creator.id, chat_guest2.id ])
      expect(edit_chat.users).to eq [@chat_creator, chat_guest2]
    end


    it 'может получить список чатов в которых он принимает участие' do
      expect(@chat_guest1.chats).to eq [@chat]
    end


    # it 'не может менять информацию о чате в котором он не участвует'
    it 'не может редактировать чат, в котором он не участвует' do
      another_user = create(:user)
      edit_chat = another_user.get_chat_by_id(@chat.id)
      expect(edit_chat).to be_nil
    end


    it 'может написать сообщение в чат, в котором он участвует' do
      msg = @chat_creator.new_message(@chat, text: 'message text')
      msg.valid?
      expect(msg).to be_valid
    end


    it 'может получить список непрочитанных сообщений' do
      msg = @chat_creator.new_message(@chat, text: 'message text')
      msg.save
      expect(@chat_guest1.unreaded_messages_for(@chat)).to include(msg)
    end


    it 'может отметить чат как прочитанный**' do
      msg = @chat_creator.new_message(@chat, text: 'message text')
      msg.save
      unread_messages_count_before = @chat_guest1.unreaded_messages_count_for(@chat)
      @chat.mark_as_readed_for(@chat_guest1)
      unread_messages_count_after = @chat_guest1.unreaded_messages_count_for(@chat)
      expect(unread_messages_count_after).to be_zero
    end

  end


end